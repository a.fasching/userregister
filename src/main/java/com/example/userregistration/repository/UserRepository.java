package com.example.userregistration.repository;

import com.example.userregistration.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Override
    List<User> findAllById(Iterable<Long> longs);

    @Override
    Optional<User> findById(Long aLong);
}
