package com.example.userregistration.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "User_data")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    private String name;
    private String email;
    private String password;
    private Date birthday;
    private Gender gender;
    private String profession;
    private boolean married;
    private String note;

    public User(String name, String email, String password, Date birthday, Gender gender, String profession, boolean married, String note) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.birthday = birthday;
        this.gender = gender;
        this.profession = profession;
        this.married = married;
        this.note = note;
    }
}
