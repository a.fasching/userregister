package com.example.userregistration.controller;

import com.example.userregistration.model.User;
import com.example.userregistration.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {
    private final UserService service;

    @GetMapping("/home/users")
    public List<User> displayUsers(){
        return service.getUser();
    }

    public ApiController(UserService service) {
        this.service = service;
    }
}
