package com.example.userregistration.controller;

import com.example.userregistration.model.User;
import com.example.userregistration.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
    private final UserService service;


    public UserController(UserService service) {
        this.service = service;

    }
    @GetMapping("/home")
    public String getHome(Model model){
        model.addAttribute("user", new User());
        return "home";
    }
    @PostMapping("/home")
    public String processRegister(User user){
        service.addUser(user);
        return "home";
    }


}
